<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../home/mini-course/vendor/autoload.php');
require(__DIR__ . '/../home/mini-course/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../home/mini-course/config/web.php');

(new yii\web\Application($config))->run();