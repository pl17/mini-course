<?php
setcookie('language', 'ru', 0, '/', '.7km-najar.com.ua');
setcookie('_ym_isad', '', time()-3600);

$pathToApp = __DIR__ . '/../../../../home/mini-course/';
require($pathToApp . 'vendor/autoload.php');
require($pathToApp . 'vendor/yiisoft/yii2/Yii.php');
$config = require($pathToApp . 'config/web.php');

(new yii\web\Application($config))->run();