SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
  ('m000000_000000_base',	1477853218),
  ('m161030_174612_create_students_table',	1478594980),
  ('m161030_175210_create_courses_completed_table',	1478594981),
  ('m161030_175751_create_courses_process_table',	1478594982);

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_key` (`auth_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `students` (`id`, `login`, `auth_key`, `updated_at`, `created_at`) VALUES
  (1,	'demo-student',	NULL,	'2016-11-08 11:49:50',	'2016-11-08 11:49:50'),
  (2,	'demo-access-student',	NULL,	'2016-11-08 11:49:50',	'2016-11-08 11:49:50'),
  (3,	'polo.ao.com',	NULL,	'2016-11-08 11:49:50',	'2016-11-08 11:49:50'),
  (4,	'user.ao.com',	NULL,	'2016-11-08 11:49:50',	'2016-11-08 11:49:50'),
  (5,	'your-secret-key-is-wrong',	NULL,	'2016-11-08 11:49:50',	'2016-11-08 11:49:50'),
  (6,	'morning-after...',	NULL,	'2016-11-08 11:49:54',	'2016-11-08 11:49:54');

DROP TABLE IF EXISTS `courses_completed`;
CREATE TABLE `courses_completed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `points` int(1) NOT NULL,
  `started_at` datetime NOT NULL,
  `completed_at` datetime NOT NULL,
  `time_spent` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-courses_completed-student_id` (`student_id`),
  CONSTRAINT `fk-courses_completed-student_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `courses_process`;
CREATE TABLE `courses_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `current_step` int(1) NOT NULL,
  `current_points` int(1) NOT NULL,
  `started_at` datetime NOT NULL,
  `closed` tinyint(1) NOT NULL,
  `contents` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-courses_process-student_id` (`student_id`),
  CONSTRAINT `fk-courses_process-student_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

