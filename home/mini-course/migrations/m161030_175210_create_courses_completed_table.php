<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses_completed`.
 */
class m161030_175210_create_courses_completed_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('courses_completed', [
            'id' => $this->primaryKey(11),
            'student_id' => $this->integer(11)->notNull(),
            'points' => $this->integer(1)->notNull(),
            'started_at' => $this->dateTime()->notNull(),
            'completed_at' => $this->dateTime()->notNull(),
            'time_spent' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk-courses_completed-student_id',
            'courses_completed',
            'student_id',
            'students',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('courses_completed');
    }
}
