<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses_process`.
 */
class m161030_175751_create_courses_process_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('courses_process', [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer(11)->notNull(),
            /* 2 ... 6 */
            'current_step' => $this->integer(1)->notNull(),
            /* 0 ... 5 */
            'current_points' => $this->integer(1)->notNull(),
            'started_at' => $this->dateTime()->notNull(),
            'closed' => $this->boolean()->notNull(),
            'contents' => $this->string(1024)
        ]);

        $this->addForeignKey(
            'fk-courses_process-student_id',
            'courses_process',
            'student_id',
            'students',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('courses_process');
    }
}
