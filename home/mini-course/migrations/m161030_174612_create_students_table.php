<?php

use yii\db\Migration;

/**
 * Handles the creation of table `students`.
 */
class m161030_174612_create_students_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('students', [
            'id' => $this->primaryKey(11),
            'login' => $this->string(32)->notNull(),
            'auth_key' => $this->string(32)->unique(),
            'updated_at' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('students');
    }
}
