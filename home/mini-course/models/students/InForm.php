<?php
/**
 * Project:     mini-course
 * File:        InForm.php
 * Author:      planet17
 * DateTime:    M10.D31.2016 7:52 PM
 */

namespace app\models\students;

use yii;
use yii\base\Model;

/**
 * InForm is the model behind the login form.
 *
 * @property Student|null $user This property is read-only from outer.
 *
 * @property string $login
 */
class InForm extends Model
{
    /** @var string $login */
    public $login;

    /** @var boolean|Student $_user */
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'exist', 'targetClass' => Student::className()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ['login' => 'Your key-login'];
    }

    /**
     * Login in a user using the provided login.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 3600 * 24 * 30);
        }
        return false;
    }

    /**
     * Finds user by [[login]]
     *
     * @return Student|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Student::findByLogin($this->login);
        }

        return $this->_user;
    }
}