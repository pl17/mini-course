<?php
/**
 * Project:     mini-course
 * File:        Persons.php
 * Author:      planet17
 * DateTime:    M10.D30.2016 11:28 PM
 */

namespace app\models\students;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\HttpException;
use yii\web\IdentityInterface;

/**
 * Class Student.
 *
 * @package app\models
 *
 * @property integer $id Student's ID.
 * @property string $login Student's Login.
 * @property string $auth_key AuthKey of that Identity.
 *
 */
class Student extends ActiveRecord implements IdentityInterface
{

    /**
     * @const TOO_SHORT_TEXT
     */
    const TOO_SHORT_TEXT = "Minimum {min, number} {min, plural, one{symbol} few{symbols} many{symbols} other{symbols}} at «{attribute}».";

    /**
     * @const TOO_LONG_TEXT
     */
    const TOO_LONG_TEXT = "Maximum {min, number} {min, plural, one{symbol} few{symbols} many{symbols} other{symbols}} at «{attribute}».";

    /**
     * @const LOGIN_STRING_LENGTH_MIN
     */
    const LOGIN_STRING_LENGTH_MIN = 7;

    /**
     * @const LOGIN_STRING_LENGTH_MAX
     */
    const LOGIN_STRING_LENGTH_MAX = 32;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%students}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Your key-login'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['login', 'filter', 'filter' => 'trim'],

            [
                ['login'],
                'required',
                'message' => 'Something wrong with {attribute}.',
                'skipOnEmpty' => false
            ],

            [
                'login',
                'string',
                'length' => [static::LOGIN_STRING_LENGTH_MIN, static::LOGIN_STRING_LENGTH_MAX],
                "tooShort" => static::TOO_SHORT_TEXT,
                "tooLong" => static::TOO_LONG_TEXT
            ],

            [
                'login',
                'unique',
                'message' => 'This login has already been used.'
            ],

            [
                'auth_key',
                'string',
                'length' => [32, 32],
                "tooShort" => static::TOO_SHORT_TEXT,
                "tooLong" => static::TOO_LONG_TEXT
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        /* @return static|null */
        return static::findOne(['id' => (integer)$id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /* String below cause we not use API authorization, but must
         * implemented that method. */
        throw new HttpException(501, 'Identity by Token is not implemented');
    }

    /**
     * That's method implementing yii\web\IdentityInterface.
     *
     * Also that method have features with updating auth_key and save new value to Database.
     *
     * For more information look description at yii\web\IdentityInterface.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     *
     * @see \yii\web\IdentityInterface::getAuthKey
     * @see Student::validateAuthKey
     */
    public function getAuthKey()
    {
        $this->generateAuthKey();
        $this->update();
        return $this->auth_key;
    }

    /**
     * It generates a hash random string for auth_key.
     *
     * Method calls by Student::getAuthKey.
     *
     * @see Student::getAuthKey
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Finds user by login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * Method create a new Student.
     * Then try to save User with provided params.
     *
     * @param string $login
     *
     * @return Student.
     *
     * @see Student.
     */
    public static function create($login)
    {
        /** @var Student $user */
        $user = new Student();
        $user->login = $login;
        return ($user->validate() && $user->save()) ? true : $user->errors;
    }

}