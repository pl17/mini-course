<?php
/**
 * Project:     mini-course
 * File:        CoursesProcess.php
 * Author:      planet17
 * DateTime:    M10.D30.2016 11:33 PM
 */
namespace app\models\courses\models;

use app\models\students\Student;

use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class CoursesProcess
 * @package app\models\courses\models
 *
 * @property integer $id
 * @property integer $student_id
 * @property integer $current_step
 * @property integer $current_points
 * @property string $started_at
 * @property boolean $closed
 * @property string $contents
 * 
 */
class CoursesProcess extends ActiveRecord
{
    /** @var null|ContentsTemplate */
    public $_contents = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'started_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%courses_process}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [
                ['student_id', 'current_step', 'current_points', 'closed', 'contents'],
                'required',
                'message' => 'Something wrong with {attribute}.',
                'skipOnEmpty' => false
            ],

            ['closed', 'boolean'],

            ['current_step', 'integer', 'min' => 2, 'max' => 7],
            ['current_points', 'integer', 'min' => 0, 'max' => 5],

            [
                'student_id',
                'exist',
                'targetClass' => Student::className(),
                'targetAttribute' => 'id'
            ]
        ];
    }

    /**
     * Method return last opened CoursesProcess for current user.
     * 
     * @return static|null
     */
    public static function getOpenByStudentID()
    {        
        $id = Yii::$app->user->identity->getId();

        $return = static::findOne([
            'student_id' => $id,
            'closed' => false
        ]);

        if ($return !== null) {
            $return->getContent();
        }

        return $return;
    }

    /**
     * Method generated template of new object with preset attributes values.
     * 
     * @return $this|boolean
     */
    public function generateNew()
    {
        $this->closed = false;
        $this->current_points = 0;
        $this->started_at = (new \DateTime())->format('Y-m-d H:i:s');
        
        $this->student_id = Yii::$app->user->identity->getId();
        $this->current_step = 2;

        $this->setNewSequence();

        if ($this->save()) {
            return $this;
        }
        
        return false;
    }

    /**
     * Method uses when need get step and shift it from array.
     *
     * If no more steps it return null.
     * 
     * @return integer|null
     */
    public function constructNextStep()
    {
        return array_shift($this->getContent()->sequence);
    }

    public function getQuestionSettings()
    {
        return $this->getContent()->temporaryQuestionSettings;
    }


    /**
     * Method set new randomly sequence of pages queue.
     * 
     */
    protected function setNewSequence()
    {
        $a = range(0, 4);
        shuffle($a);

        $this->getContent()->sequence = $a;
    }

    /**
     * Method get object Content like ContentsTemplate class.
     * 
     * @return ContentsTemplate
     */
    public function getContent()
    {
        if ($this->_contents === null) {
            $this->_contents = new ContentsTemplate();

            if ($this->contents !== null) {
                $getInfo = json_decode($this->contents);

                $this->_contents->sequence =
                    (isset($getInfo->sequence)) ?
                        $getInfo->sequence : [];

                $this->_contents->temporaryQuestionSettings =
                    (isset($getInfo->temporaryQuestionSettings)) ?
                        $getInfo->temporaryQuestionSettings : null;
            }

        }
        return $this->_contents;
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->contents = $this->getContent()->__toString();
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @inheritdoc
     */
    public function update($runValidation = true, $attributeNames = null)
    {
        $this->contents = $this->getContent()->__toString();
        return parent::update($runValidation, $attributeNames);
    }
}