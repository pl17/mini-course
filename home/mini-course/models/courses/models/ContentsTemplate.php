<?php
/**
 * Project:     mini-course
 * File:        ContentsTemplate.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 1:45 AM
 */

namespace app\models\courses\models;
/**
 * Class ContentsTemplate
 * @package app\models\courses\models.
 * 
 * Just template.
 */
class ContentsTemplate
{
    public $sequence = [];
    /** @var temporaryQuestionSettingsTemplate */
    public $temporaryQuestionSettings = null;

    /**
     * @return mixed
     */
    public function __toString()
    {
        return json_encode($this);
    }
}

/**
 * Class temporaryQuestionSettingsTemplate.
 *
 * @package app\models\courses\models.
 *
 * @property integer $typePointer
 * @property string $className
 * @property string $templateName
 * @property temporaryQuestionSettingsContentTemplate $content
 */
class temporaryQuestionSettingsTemplate
{
    public $typePointer;
    public $className;
    public $templateName;
    public $content;
}

/**
 * Class temporaryQuestionSettingsContentTemplate.
 * @package app\models\courses\models
 *
 * @property string $question
 * @property integer $nextStep
 */
class temporaryQuestionSettingsContentTemplate
{
    public $question;
    public $nextStep;
}