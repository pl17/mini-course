<?php
/**
 * Project:     mini-course
 * File:        Result.php
 * Author:      planet17
 * DateTime:    M10.D30.2016 11:32 PM
 */
namespace app\models\courses\models;

use app\models\students\Student;

use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Result
 * @package app\models\courses\models
 *
 * @property integer $id ID of Result's row.
 * @property integer $student_id ID of Student what take a part at Course.
 * @property integer $points Points what get student through the Course.
 *
 * @property string $started_at Time when Course had started.
 * @property string $completed_at Time when Course had completed.
 *
 * @property integer $time_spent Time spent at the Course at seconds.
 */
class Result extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'completed_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()')
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'time_spent',
                'updatedAtAttribute' => false,
                'value' => new Expression(
                    'UNIX_TIMESTAMP(`completed_at`) - UNIX_TIMESTAMP(`started_at`)'
                )
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%courses_completed}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [
                ['student_id', 'points', 'started_at'],
                'required',
                'message' => 'Something wrong with {attribute}.',
                'skipOnEmpty' => false
            ],

            ['points', 'integer', 'min' => 0, 'max' => 5],

            [
                'student_id',
                'exist',
                'targetClass' => Student::className(),
                'targetAttribute' => 'id'
            ],

            ['started_at', 'date', 'format' => 'php:Y-m-d H:i:s']
        ];
    }

    /**
     * Method return last results.
     * Amount provided by param.
     * If param no set, then uses default 10.
     *
     * @param integer $amount
     * @return null|Result[]
     */
    public function getLastResults($amount = 10)
    {
        $amount = (integer)$amount;

        $return = static::find()
            ->orderBy(['completed_at' => SORT_DESC])
            ->limit($amount)->all();

        return $return;
    }

    /**
     * Method create new Course via CourseProcess object.
     */
    public function createByCourseProcessObject()
    {
        $this->student_id = Yii::$app->course->info->student_id;
        $this->points = Yii::$app->course->info->current_points;
        $this->started_at = Yii::$app->course->info->started_at;
        
        
        $this->save();
    }

    public function getLastResultForCurrentStudent()
    {
        return static::find()->where(
            ['student_id' => Yii::$app->user->identity->getId()]
        )->orderBy(['completed_at' => SORT_DESC])->one();
    }

    /**
     * @return null|Student
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id'])->one();
    }
}