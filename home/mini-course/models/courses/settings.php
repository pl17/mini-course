<?php
/**
 * Project:     mini-course
 * File:        settings.php
 * Author:      planet17
 * DateTime:    M11.D04.2016 2:01 AM
 */
return [

    0 => [
        'type' => 'read',
        'task' => [
            'text' => 'Hello! It is just text! If you read it, and go to next page you will get 1 point!'
        ]
    ],
    
    1 => [
        'type' => 'input',
        'model' => 'QuestionSumRandom',
        'task' => [
            'dataGeneratorRules' => ['order' => [
                'options',
                'getRight',
                'question'
            ]],

            'text' => [
                'Calculate the result:<br>',
                [['getTextForSumBy', []]]
            ],
            'answers' => [

                'options' => [
                    'dataGeneratorRules' =>
                        [['generateRand', [10, 99],[10, 99]]],
                    'quantity' => 2,
                ],
                'right' => ['value' => 'function sumOfAll']
            ]
        ]
    ],
    
    2 => [
        'type' => 'radio',
        'model' => 'QuestionToday',
        'task' => [
            'dataGeneratorRules' => ['order' => [
                'getRightOption',
                'helper',
                'options',
                'question'
            ]],

            'right' => [
                'origin' => [],
                'value' => null,
                'position' => null
            ],


            'options' => [
                'rules' => ['shuffle'],
                'quantity' => 4,
            ],

            'helper' => [
                'function constructHelperInformationByTime',
                'Attention at server time it calculated at server so current time.',
                'Question had constructed at time.'
            ],

            'text' => ['What day of the week now?'],

        ]
    ],
    3 => [
        'type' => 'checkbox',
        'model' => 'QuestionProgrammingLanguage',
        'task' => [
            'text' => [
                'What programming languages do you know?'
            ],

            'dataGeneratorRules' => [
                'order' => [
                    'options',
                    'question'
                ]
            ],
            
            'options' => [
                'rules' => ['shuffle'],
                'data' =>
                    [
                        'PHP',
                        'Python',
                        '.net',
                        'JS',
                        'VisualBasic'
                    ],                
                'quantity' => 5,
            ],
            'right' => [
                'value' => 'VisualBasic',
                'position' => 'RememberPosition(VisualBasic)',
                'functions' => [
                    'countSelected',
                    'notIn',
                ]
            ]
        ]
    ],

    4 => [
        'type' => 'video',
        'task' => [
            'text' => 'Watch the video before the end of',
            /* https://www.youtube.com/watch?v=DRQfPpx1EPA */
            'src' => 'DRQfPpx1EPA'
        ]
    ],
];