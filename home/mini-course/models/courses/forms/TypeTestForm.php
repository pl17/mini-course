<?php
/**
 * Project:     mini-course
 * File:        TypeTestGeneralForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 9:00 PM
 */
namespace app\models\courses\forms;

class TypeTestGeneralForm extends BaseGeneralForm
{
    public $test = 'angel';
}