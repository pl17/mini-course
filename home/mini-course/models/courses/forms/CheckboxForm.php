<?php
/**
 * Project:     mini-course
 * File:        CheckboxForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 2:57 PM
 */
namespace app\models\courses\forms;

use yii;

/**
 * Class CheckBoxForm.
 *
 * Form for Page marked 3.
 * 
 * @package app\models\courses\forms
 */
class CheckboxForm extends BaseAnswerForm
{


    // TODO Implement it!!!

    // scenario
    // full
    //      -> anyIn (_0,+1,2,3),
    //      -> anyNotIn (_0,1,!2,3)
    //      -> function([$positions], [$low_selected_limit], )
    // improve that way...

    // properties configuration
    
    // minimal selected for positive result
    // maximal selected for positive result
    // type: [percent|full|mathFormulaFn|preSetConfigurableState]
    // data options [list|generateFn]    
    // right options
    

    /*
     * the execution order:
     * 1) construct options
     * 2) construct question
     * 3) construct saveNewConfiguration
     *

    'task' => [
            'text' => [
                'What programming languages you know?'
            ],



            'options' => [
                'dataGeneratorRules' =>
                    [
                        'shuffle',
                    ],

                'data' =>
                    [
                        'PHP',
                        'Python',
                        '.net',
                        'JS',
                        'VisualBasic'
                    ],
                // if isset data does not uses quantity
                'quantity' => 5,
            ],
            'right' => [
                'value' => 'VisualBasic',
                'position' => 'RememberPosition(VisualBasic)',
                'functions' => [
                    'countSelected',
                    'notIn',
                ]
            ]
        ]

     */
}