<?php
/**
 * Project:     mini-course
 * File:        QuestionProgrammingLanguageCheckboxForm.php
 * Author:      planet17
 * DateTime:    M11.D07.2016 5:56 AM
 */

namespace app\models\courses\forms;

use yii;

/**
 * Class QuestionProgrammingLanguageCheckboxForm
 * @package app\models\courses\forms
 */
class QuestionProgrammingLanguageCheckboxForm extends CheckboxForm
{
    /**
     * @var mixed $selected
     */
    public $selected;

    protected $_selectedCount;

    private function preSetDataList()
    {
        return [
            'PHP',
            'Python',
            '.net',
            'JS',
            'VisualBasic'
        ];
    }

    private function rightOptions()
    {
        return [
            'PHP',
            'Python',
            '.net',
            'JS',
        ];
    }

    private function rulesConstructorsOrder()
    {
        return [
            'options',
            'question'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['selected', 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @param $refSrc
     * @param $data
     * @return void
     */
    public function prepareContent(&$refSrc, &$data)
    {
        parent::prepareModel($refSrc);

        if (
            isset(Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared)
            && Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared === true
        ) {
            $this->loadFromContent($refSrc);
            return;
        }

        $this->runConstructors($data->task);

        Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared = true;
        $this->loadFromContent($refSrc);
    }

    /**
     * @param object $task
     */
    protected function runConstructors($task)
    {
        foreach ($this->rulesConstructorsOrder() as $constructorName) {
            $functionName = $constructorName . 'Constructor';
            call_user_func_array([$this, $functionName], [$task]);
        }
    }

    protected function optionsConstructor($task)
    {
        $content = &Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings
            ->content;

        $content->right = json_decode(json_encode(['values' => null, 'positions' => null]));

        Yii::$app->course->info->getContent()->temporaryQuestionSettings
            ->content->right->values = $this->rightOptions();

        $options = $this->preSetDataList();
        shuffle($options);

        foreach ($this->rightOptions() as $value) {
            Yii::$app->course->info->getContent()->temporaryQuestionSettings
                ->content->right->positions[] = array_search($value, $options);
        }

        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->options = $options;

    }

    protected function questionConstructor($task)
    {
        $question = 'What programming languages do you know?';
        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->question = $question;
    }

    /**
     * @param $refSrc
     * @return void
     */
    protected function loadFromContent(&$refSrc)
    {
        $refSrc->question = Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->question;
    }

    protected function onCheckSuccess()
    {
        if ($this->checkIfRightSelection()) {
            $this->addPoints();
        }
    }

    private function checkIfRightSelection()
    {
        $this->_selectedCount = count($this->selected);
        if (!$this->_selectedCount) {
            return false;
        }

        $positions = Yii::$app->course->info->_contents
            ->temporaryQuestionSettings->content->right->positions;

        foreach ($this->selected as $selected) {

            $r = array_search($selected, $positions);

            if (false === $r) {
                return false;
            }

        }

        return true;
    }
}