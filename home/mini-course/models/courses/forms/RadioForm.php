<?php
/**
 * Project:     mini-course
 * File:        RadioForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 2:58 PM
 */
namespace app\models\courses\forms;

use yii;

/**
 * Class RadioForm.
 *
 * Form for Page marked 2.
 *
 * @package app\models\courses\forms.
 */
class RadioForm extends BaseAnswerForm
{
    public function prepareContent(&$refSrc, &$data)
    {
        parent::prepareContent($refSrc, $data);
    }

    /**
     * @param object $task
     */
    protected function runConstructors($task)
    {
        if (isset($task->dataGeneratorRules)) {
            $order = $task->dataGeneratorRules->order;

            foreach ($order as $constructorName) {
                $functionName = $constructorName . 'Constructor';
                call_user_func_array([$this, $functionName], [$task]);
            }
        }
    }

    protected function getRightOptionConstructor($task)
    {
        
    }

    protected function helperConstructor($task)
    {
        
    }

    protected function optionsConstructor($task)
    {
        
    }

    protected function questionConstructor($task)
    {
        
    }

    // TODO Implement it!!!

    /*
     * the execution order:
     * 0) constructHelperInformationByTime();
     * 1) generate options by func
     * 2) 
     * jddayofweek (0,1);
     * 1) getTodayName($date)
     * 2) storage['value']
     * 
     
    'text' => [
                'What day of the week now?'
            ],
            'helper' => [
                'function constructHelperInformationByTime',
                'Attention at server time it calculated at server so current time.',
                'Question had constructed at time.'
            ],
            'options' => [
                'dataGeneratorRules' =>
                    [
                        [['generateRandomsWithOne', []]],
                        [['shuffle', []]],
                    ],
                'quantity' => 4,
            ],
            'right' => [
                'value' => [['getTodayName', []]],
                'position' => 'function FindKeyInArrayRememberPosition',
            ]
     
     */
}