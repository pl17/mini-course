<?php
/**
 * Project:     mini-course
 * File:        BaseAnswerForm.php
 * Author:      planet17
 * DateTime:    M11.D06.2016 11:18 AM
 */
namespace app\models\courses\forms;
use yii\base\Exception;

/**
 * Class BaseAnswerForm
 * @package app\models\courses\forms
 * 
 *
 *
 * @see BaseAnswerForm::prepareContent
 * 
 * @see BaseGeneralForm
 * @see yii\base\Model
 * 
 * @see yii\base\Model::rules
 *
 * @see BaseGeneralForm::points - Points what been get if Student answer is right.
 *
 * @see BaseGeneralForm::rules
 * @see BaseGeneralForm::check
 * @see BaseGeneralForm::onCheckSuccess
 *
 * @see BaseGeneralForm::prepareContent
 *
 * @see BaseGeneralForm::addPoints
 * @see BaseGeneralForm::incrementStepAndSaveCourseInfo
 */
class BaseAnswerForm extends BaseGeneralForm
{
    
    /**
     * @inheritdoc
     *
     * Get and modify data!
     */
    public function prepareContent(&$refSrc, &$data)
    {
        parent::prepareModel($refSrc);
    }

    /*
    /**
     * @param $refSrc
     * @param $data
     * @return mixed
     * @throws Exception
     * 
       $this->prepareQuestion($refSrc, $data);
     * 
     * /
    public function prepareQuestion(&$refSrc, $data)
    {
        if (is_string($data->question)) {
            return $refSrc->question = $data->question;
        }

        if (!is_array($data->question)) {
            throw new Exception('Wrong type of provided params with Courses Settings');
        }

        $refSrc->question = '';

        foreach ($data->question as $questionItem) {

            if (is_string($data->question)) {
                $refSrc->question .= $questionItem;
            } elseif (is_array($questionItem)) {

                foreach ($questionItem as $functionObject) {
                    if(!is_array($functionObject)) {
                        throw new Exception('Wrong type of provided params with Courses Settings');
                    }

                    $functionName = array_shift($functionObject);

                    foreach ($functionObject as $callParams) {
                        $refSrc->question .= 'call_user_func_array([$this, $functionName], $callParams)';
                    }
                }
            }
        }

        return true;
    } */
}