<?php
/**
 * Project:     mini-course
 * File:        ReadForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 2:58 PM
 */
namespace app\models\courses\forms;

/**
 * Class ReadForm.
 *
 * Form for Page marked 0.
 *
 * @package app\models\courses\forms
 *
 * @property $hiddenJack
 */
class ReadForm extends BaseReadingForm
{
    /**
     * @inheritdoc
     */
    protected function onCheckSuccess()
    {
        $this->addPoints();
    }
}