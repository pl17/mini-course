<?php
/**
 * Project:     mini-course
 * File:        CustomStartForm.php
 * Author:      planet17
 * DateTime:    M11.D02.2016 4:10 PM
 *
 */
namespace app\models\courses\forms;

use yii;

/**
 * Class CustomStartForm
 * @package app\models\courses\forms
 */
class CustomStartForm extends BaseReadingForm
{

    protected function onCheckSuccess()
    {
        /*
         * Script separate to one public method.
         * Cause need public access for same script in other behaviours. 
         * */
        $this->runConstructor();
    }

    /**
     * Public function for initialize new Course at Student.
     *
     * Direct method for access from outer.
     *
     * At first that check if exist any still opened Courses at Student.
     *
     * If no any Course exist - then run generator of Course.
     */
    public function runConstructor()
    {
        if (!Yii::$app->course->checkIfOpenCourseExistByStudent()) {
            Yii::$app->course->setNewCourse();
        }
    }
}