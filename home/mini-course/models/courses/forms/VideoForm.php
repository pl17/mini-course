<?php
/**
 * Project:     mini-course
 * File:        VideoForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 2:56 PM
 */
namespace app\models\courses\forms;

/**
 * Class VideoForm.
 *
 * Form for Page marked 4.
 *
 * @package app\models\courses\forms
 *
 * @property $hiddenJack
 */
class VideoForm extends BaseReadingForm
{
    /**
     * @inheritdoc
     */
    protected function onCheckSuccess()
    {
        $this->hiddenJack = (boolean)$this->hiddenJack;

        if ($this->hiddenJack) {
            $this->addPoints();
        }
    }

    /**
     * @inheritdoc
     */
    public function prepareContent(&$refSrc, $data)
    {
        parent::prepareContent($refSrc, $data);
        $refSrc->videoLink = $data->task->src;
    }
}