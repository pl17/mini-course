<?php
/**
 * Project:     mini-course
 * File:        BaseGeneralForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 3:09 PM
 */
namespace app\models\courses\forms;

use app\models\courses\models\CoursesProcess;

use yii;
use yii\base\Model;

/**
 * Class BaseGeneralForm
 * @package app\models\courses\forms
 *
 * @see yii\base\Model
 * @see yii\base\Model::rules
 * 
 * @see BaseGeneralForm::points - Points what been get if Student answer is right.
 * 
 * @see BaseGeneralForm::rules
 * @see BaseGeneralForm::check
 * @see BaseGeneralForm::onCheckSuccess
 * 
 * @see BaseGeneralForm::prepareContent
 * 
 * @see BaseGeneralForm::addPoints
 * @see BaseGeneralForm::incrementStepAndSaveCourseInfo
 */
class BaseGeneralForm extends Model
{
    /**
     * @var integer $points How much points you get for that type of question.
     *
     * @see BaseGeneralForm         - Current class.
     */
    protected $points = 1;
    
    /**
     * @inheritdoc
     *
     * @see yii\base\Model::rules - Parent (Origin) of that method..
     * @see BaseGeneralForm         - Current class.
     */
    public function rules()
    {
        return parent::rules();
    }

    /**
     * Method return a boolean.
     * 
     * @return boolean.
     * 
     * @see BaseGeneralForm         - Current class.
     */
    public function check()
    {
        if (!$this->validate()) {
            return;
        }
        
        $this->onCheckSuccess();
    }

    /**
     * Method what will calls if all level of check() return true.
     * Override it any child method if need change handler behaviours,
     *
     * @see BaseGeneralForm::check()    - ...
     * @see BaseGeneralForm             - Current class.
     */
    protected function onCheckSuccess()
    {
    }
    
    
    /**
     * Method prepare data and set it to currentStepQuestionLayer.
     * 
     * @param object $refSrc Reference to object
     * CoursesComponent::currentStepQuestionLayer->content.
     * 
     * 
     * @see BaseGeneralForm         - Current class.
     */
    public function prepareModel(&$refSrc)
    {
        $refSrc->model = $this;
    }

    /* region methods helpers working with Component *********************** */
    
    /**
     * Method helper Add Points to Course information.
     * 
     * @see CoursesProcess          - Object what contain all points of Course.
     * @see BaseGeneralForm::points - Points what added to CoursesProcess.
     * @see BaseGeneralForm         - Current class.
     */
    protected function addPoints()
    {
        Yii::$app->course->info->current_points += $this->points;
    }   

    /* endregion methods helpers working with Component ******************** */
}