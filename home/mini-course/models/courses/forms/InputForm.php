<?php

/**
 * Project:     mini-course
 * File:        NumberInputForm.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 2:58 PM
 */
namespace app\models\courses\forms;

use yii;
use yii\helpers\HtmlPurifier;

/**
 * Class NumberInputForm.
 *
 * Form for Page marked 1.
 *
 * @package app\models\courses\forms.
 *
 * @see InputGeneralForm::field
 * @see InputGeneralForm::rules
 * @see InputGeneralForm::secureFilterInput
 * @see InputGeneralForm::onCheckSuccess
 * @see InputGeneralForm::prepareContent
 *
 * @see BaseAnswerForm
 * @see BaseGeneralForm
 * @see yii\base\Model
 *
 * @see yii\base\Model::rules
 * @see BaseGeneralForm::rules
 *
 * @see BaseGeneralForm::check
 * @see BaseGeneralForm::onCheckSuccess
 *
 * @see BaseGeneralForm::prepareContent
 *
 * @see BaseGeneralForm::addPoints
 * @see BaseGeneralForm::incrementStepAndSaveCourseInfo
 * @see BaseGeneralForm::points - Points what been get if Student answer is right.
 * 
 */
class InputForm extends BaseAnswerForm
{
    public $field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [
                ['field'],
                'required',
                'message' => 'Fill field {attribute}.',
                'skipOnEmpty' => false
            ],
            [
                ['field'],
                'filter',
                'filter' => 'trim'
            ],

            [
                ['field'],
                'integer'
            ],

            [
                ['field'],
                'secureFilterInput'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ['field' => 'Fill answer...'];
    }

    /**
     * Simple XSS-filter.
     *
     * @param $attribute
     * @return bool
     */
    public function secureFilterInput($attribute)
    {
        $this->$attribute = HtmlPurifier::process($this->$attribute);
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function onCheckSuccess()
    {
        $this->field = (boolean)$this->field;

        if ($this->field) {
            $this->addPoints();
        }
    }

    /**
     * @inheritdoc
     */
    public function prepareContent(&$refSrc, &$data)
    {
        //parent::prepareContent($refSrc, $data);
        /*
         * Answers parser
         *
         * the execution order:
         * 
         * 1) get Options
         * 2) get Text
         * 3) get Right

        'text' => [
                'Calculate the result:<br>',
                [['getTextForSumBy', []]]
            ],
            'answers' => [
                'options' => [
                    'dataGeneratorRules' =>
                        [['generateRand', [10, 99],[10, 99]]],
                    'quantity' => 2,
                ],
                'right' => ['value' => 'function sumOfAll']
            ]

         */
    }
}