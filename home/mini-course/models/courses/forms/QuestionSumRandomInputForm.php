<?php
/**
 * Project:     mini-course
 * File:        QuestionSumRandomInputForm.php
 * Author:      planet17
 * DateTime:    M11.D07.2016 6:27 PM
 */

namespace app\models\courses\forms;

use yii;

class QuestionSumRandomInputForm extends InputForm
{
    public $field;

    private function rulesConstructorsOrder()
    {
        return [
            'options',
            //'gerRight'
            'question'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['field', 'required'],
            ['field', 'integer']
        ];
    }

    /**
     * @param $refSrc
     * @param $data
     * @return void
     */
    public function prepareContent(&$refSrc, &$data)
    {
        parent::prepareModel($refSrc);

        if (
            isset(Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared)
            && Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared === true
        ) {
            $this->loadFromContent($refSrc);
            return;
        }

        $this->runConstructors($data->task);

        Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared = true;
        $this->loadFromContent($refSrc);
    }

    /**
     * @param object $task
     */
    protected function runConstructors($task)
    {
        foreach ($this->rulesConstructorsOrder() as $constructorName) {
            $functionName = $constructorName . 'Constructor';
            call_user_func_array([$this, $functionName], [$task]);
        }
    }

    protected function optionsConstructor($task)
    {
        $options = [];

        $sum = 0;

        for ($i = 2; $i--;) {
            $sum += $number = $this->generateRandomNumber();
            array_push($options, $number);
        }

        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->options = $options;

        $right = json_decode(json_encode([
            'value' => $sum
        ]));

        Yii::$app->course->info->getContent()->temporaryQuestionSettings
            ->content->right = $right;

    }

    protected function questionConstructor($task)
    {
        $question = 'Calculate the result:<br>' .
            join(' + ', Yii::$app->course->info->getContent()
                ->temporaryQuestionSettings->content->options) . ' =';

        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->question = $question;
    }

    /**
     * @param $refSrc
     * @return void
     */
    protected function loadFromContent(&$refSrc)
    {
        $refSrc->question = Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->question;
    }

    protected function onCheckSuccess()
    {
        $this->field = (integer)$this->field;

        if ($this->field === Yii::$app->course->info->getContent()
                ->temporaryQuestionSettings->content->right->value
        ) {
            $this->addPoints();
        }
    }

    /**
     * @return integer
     */
    private function generateRandomNumber()
    {
        // sketch for by length:
        // min get by length '1' + '0'
        // max get string 1st + '0'
        // parse 1st and parse 2nd and decrement
        return rand(10, 99);
    }
}