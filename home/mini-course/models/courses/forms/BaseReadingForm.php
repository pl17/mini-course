<?php
/**
 * Project:     mini-course
 * File:        BaseReadingForm.php
 * Author:      planet17
 * DateTime:    M11.D06.2016 11:17 AM
 */
namespace app\models\courses\forms;

/**
 * Class BaseReadingForm
 * @package app\models\courses\forms
 *
 * @property @hiddenJack
 * 
 *
 * @see yii\base\Model
 * @see yii\base\Model::rules
 *
 * @see BaseReadingForm::hiddenJack
 * @see BaseGeneralForm::points     - Points what been get if Student answer is right.
 *
 * @see BaseGeneralForm::rules
 * @see BaseGeneralForm::check
 * @see BaseGeneralForm::onCheckSuccess
 *
 * @see BaseGeneralForm::prepareContent
 *
 * @see BaseGeneralForm::addPoints
 * @see BaseGeneralForm::incrementStepAndSaveCourseInfo
 */
class BaseReadingForm extends BaseGeneralForm
{
    /**
     * @var mixed $hiddenJack Just a hidden field what use at HiddenInput
     * It may sets to any type of value for send it to "action".
     *
     * In some case uses like hidden flag of current state of some Student action.
     *
     * Like example: At VideoForm where means whether the video had been viewed.
     */
    public $hiddenJack;

    /**
     * @inheritdoc
     *
     * @see yii\base\Model::rules   - Parent (Origin) of that method.
     * @see BaseGeneralForm::rules  - Parent (First) of that method.
     *
     * @see yii\base\Model          - Parent (Origin) with that method.
     * @see BaseGeneralForm         - Parent (First) with that method.
     * @see BaseReadingForm         - Current class.
     */
    public function rules()
    {
        return [['hiddenJack', 'required']];
    }


    /**
     * @inheritdoc
     * 
     * Just get static data!
     */
    public function prepareContent(&$refSrc, $data)
    {
        parent::prepareModel($refSrc);
        
        $refSrc->question = $data->content->question;
    }
}