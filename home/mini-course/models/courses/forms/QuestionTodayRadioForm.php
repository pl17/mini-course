<?php
/**
 * Project:     mini-course
 * File:        QuestionTodayRadioForm.php
 * Author:      planet17
 * DateTime:    M11.D07.2016 5:56 AM
 */

namespace app\models\courses\forms;

use app\models\courses\HelperFunctions;

use yii;

class QuestionTodayRadioForm extends RadioForm
{
    public $field;

    private function rulesConstructorsOrder()
    {
        return [
            'getRightOption',
            'helper',
            'options',
            'question'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['field', 'required'],
            ['field', 'integer']
        ];
    }

    /**
     * @param $refSrc
     * @param $data
     * @return void
     */
    public function prepareContent(&$refSrc, &$data)
    {
        parent::prepareModel($refSrc);

        if (
            isset(Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared)
            && Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared === true
        ) {
            $this->loadFromContent($refSrc);
            return;
        }

        $this->runConstructors($data->task);

        Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->fullPrepared = true;
        $this->loadFromContent($refSrc);
    }

    /**
     * @param object $task
     */
    protected function runConstructors($task)
    {
        foreach ($this->rulesConstructorsOrder() as $constructorName) {
            $functionName = $constructorName . 'Constructor';
            call_user_func_array([$this, $functionName], [$task]);
        }
    }

    /**
     * @param $task
     * @return void
     */
    protected function getRightOptionConstructor($task)
    {
        $date = new \DateTime();

        $right = json_decode(json_encode([
            'origin' => $date->format('jS M, H:i'),
            'value' => ($date->format('N') - 1),
            'position' => null
        ]));

        Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->right = $right;
    }

    /**
     * @param $task
     * @return void
     */
    protected function helperConstructor($task)
    {
        $helperString = 'Be careful! The answer is computed by generating' .
            ' a question of time!<br>Server time:' .
            Yii::$app->course->info->getContent()
                ->temporaryQuestionSettings->content->right->origin;

        Yii::$app->course->info->getContent()->temporaryQuestionSettings
            ->content->helper = $helperString;
    }

    protected function optionsConstructor($task)
    {
        $value = Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->right->value;

        $options = HelperFunctions::generateWeekDaysSet(4, true, 2, $value);

        $value = HelperFunctions::processingValueToNameOfWeekDay($value);

        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->right->position =
            array_search($value, $options);

        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->options = $options;

    }

    protected function questionConstructor($task)
    {
        $question = 'What day of the week now?';
        Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->question = $question;
    }

    /**
     * @param $refSrc
     * @return void
     */
    protected function loadFromContent(&$refSrc)
    {
        $refSrc->question = Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->question;

        $refSrc->helper = Yii::$app->course->info->getContent()
            ->temporaryQuestionSettings->content->helper;
    }

    protected function onCheckSuccess()
    {
        $this->field = (integer)$this->field;
        $position = (integer)Yii::$app->course->info->_contents
            ->temporaryQuestionSettings->content->right->position;

        if ($this->field === $position) {
            $this->addPoints();
        }
    }
}