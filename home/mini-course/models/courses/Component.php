<?php
/**
 * Project:     mini-course
 * File:        Component.php
 * Author:      planet17
 * DateTime:    M10.D30.2016 11:30 PM
 */

namespace app\models\courses;

use app\controllers\CourseController;
use app\models\courses\forms\BaseGeneralForm;
use app\models\courses\models\CoursesProcess;

use app\models\courses\models\Result;
use yii;
use yii\helpers\Url;
use yii\base\Exception;

/**
 * Class Component.
 *
 * @package app\models\courses.
 *
 * @property null|CoursesProcess $info - Current active course object.
 *
 * @see app\models\courses\Component::fixBy
 * @see app\models\courses\Component::secureValidation
 * @see app\models\courses\Component::validateCaseLastCourseEmpty
 * @see app\models\courses\Component::validationIfNoExpired
 * @see app\models\courses\Component::validationIfHackingSteps
 * @see app\models\courses\Component::setNewCourse
 */
class Component extends yii\base\Component
{
    /** @var array $settings */
    private $settings;

    /** @var array $currentQuestionSetting */
    private $currentQuestionSetting;

    /**
     * @const LINK_TO_CONTINUE_COURSE
     */
    const LINK_TO_COURSE_CONTINUE = '/continue-course/';
    /**
     * @const LINK_TO_COURSE_EXPIRED
     */
    const LINK_TO_COURSE_EXPIRED = '/expired/';
    /**
     * @const FORMS_NAMESPACE_PATH
     */
    const FORMS_NAMESPACE_PATH = 'app\\models\\courses\\forms\\';

    /** @var null|CoursesProcess $info */
    public $info = null;

    /** @var null|CourseController $controllerRef */
    protected $controllerRef = null;

    /** @var null|string $actionId */
    protected $actionId = null;

    /** @var boolean $isAnswer */
    protected $isAnswer = false;

    /** @var mixed $currentStepQuestionLayer */
    protected $currentStepQuestionLayer;

    /** @var integer $currentTypePointer */
    protected $currentTypePointer;

    /**
     * Component constructor.
     */
    public function __construct()
    {
        parent::__construct([]);
        $this->settings = require_once(__DIR__ . '/settings.php');
    }

    /**
     * @param yii\base\Action $a
     * @return $this
     */
    public function fixBy($a)
    {
        $this->controllerRef = $a->controller;
        $this->actionId = $a->id;
        return $this;
    }

    /**
     * Method get provided DateInterval and check if more then time
     * what set at condition.
     *
     * If more or equal then method return true.
     *
     * Now method check if Course older or and equal to three hours.
     *
     * @param \DateInterval $interval
     * @return boolean
     */
    private function checkIfMoreThenLimitedTime($interval)
    {
        return ($interval->invert === 1 && $interval->h >= 3);
    }

    /**
     * Method check if hacking or clients mistakes, or not finished course,
     * or else.
     *
     * If need method call redirect via send (No need return!).
     *
     * If all good - return true for access continue run controller's action.
     *
     *
     * @return boolean
     * @throws Exception
     */
    public function secureValidation()
    {
        /* #MARKS 4 REWRITE. Be good to rewrite it with scenario on something
         * like that. */
        /* Now it looks like a bullshit for understanding. */

        /* Case when we does not need some checks */
        if (Yii::$app->user->isGuest) {
            return true;
        }

        if ($this->actionId === 'expired' || $this->actionId === 'complete') {
            return true;
        }

        if (!$this->checkIfOpenCourseExistByStudent()) {
            return $this->validateCaseLastCourseEmpty();
        }

        /* Next methods return nothing.
         * But if something wrong then do redirect. */
        $this->validationIfNoExpired();

        /* In case when go to page Continue:
        * Return after getting info about Course.
        * Does not need next validations. */
        if ($this->actionId === 'continue-course') {
            return true;
        }

        $this->validationIfHackingSteps();

        /* If nothing redirect or stopped script, then return access for Action. */
        return true;
    }

    /**
     * @return boolean
     */
    protected function validateCaseLastCourseEmpty()
    {
        /* When no courses, check or it startPage */
        if ($this->actionId === 'start' || $this->actionId === 'expired') {
            /* When yes continue */
            return true;
        }

        /* When no, then do redirect. No need return. */
        $this->controllerRef->redirect('/', 307)->send();
        /* return false; */
    }

    /**
     * Method return via call redirect or nothing.
     * @throws Exception
     */
    protected function validationIfNoExpired()
    {
        /* validate for no expired */
        $cT = new \DateTime();

        /** @var \DateTime $cT DateTime of Course's start. */
        $timeAtStart = new \DateTime($this->info->started_at);

        if ($cT < $timeAtStart) {
            throw new Exception('Server FAILED! Something wrong at time ' .
                'configurations.');
        }

        /** @var \DateInterval $c */
        $c = $cT->diff($timeAtStart);

        if ($this->checkIfMoreThenLimitedTime($c)) {
            $this->info->closed = true;
            $this->info->update();
            $this->redirectCaseExpired();
            /* return false; */
        }
    }

    /**
     * Method return via call redirect or nothing.
     * Validation for no hacking at steps.
     *
     * @throws Exception
     */
    protected function validationIfHackingSteps()
    {
        /** @var yii\web\Request $request */
        $request = Yii::$app->request;
        /* Check for step if it current, or next */
        $currentStep = (integer)Yii::$app->request->get('step', 1);

        /* In Forms actions preset for "Next" ("Current" + 1) step.
         * That is way do not make mistakes at validation when
         * data sending by post from "Previous" step.
         *
         * Also! For next processes into script remember $request->isPost value.
         * */
        if ($this->isAnswer = $request->isPost) {
            $currentStep--;
        }

        if ($request->isGet || $this->isAnswer) {
            if ($this->info->current_step === $currentStep) {
                /* Break if all is good! */
                return;
            }
        }

        /* If script not break before - call redirect. */
        $this->redirectToContinue();
        /* return false; */
    }

    /**
     * Method check if current Student has an open Course.
     * 
     * It return true if Course exist.
     * 
     * It return false if Course not exist.
     *  
     * @deprecated
     * In next version must be rewritten!
     * 
     * Need improving method cause it can call some
     * parts of scripts more than one time.
     * 
     * So be care if you want to change something in my scripts.
     * 
     * @return boolean.
     */
    public function checkIfOpenCourseExistByStudent()
    {
        /* #MARKS 4 REWRITE. Be good to set info to null if does not exist. */
        /* Now it can call getOpenByStudentID 1 or 2 time. It's not well. */
        if ($this->info === null) {
            $this->info = CoursesProcess::getOpenByStudentID();
        }

        return ($this->info !== null);
    }

    /**
     * Create new CoursesProcess
     *
     * New CoursesProcess sets to property Component::info.
     *
     * @see app\models\courses\Component::info.
     * @see CoursesProcess.
     */
    public function setNewCourse()
    {
        /** @var CoursesProcess $model */
        $model = new CoursesProcess();
        $model->generateNew();
        $this->redirectToFirstTask();
    }

    /**
     * Method run methods what initialize Question's data and Model
     * before it can uses in anywhere.
     *
     * If processFlag set by false, it preparing all data and model only
     * for uses.
     *
     * In that case data does not need been saved.
     *
     * If processFlag set by true, it preparing all data and model
     * before rendering, it maybe called when construct first step,
     * also after processing post and getting next step,
     * restart step by get.
     *
     * In some cases data may be new, so need save it.
     *
     * Method return true if all works success.
     *
     * Method return false if something wrong.
     *
     * Method return false if no more steps for constructing.
     *
     * @param boolean $processFlag Flag for type of preparing.
     * @return bool
     */
    private function runSetToBuildQuestion($processFlag = false)
    {
        if ($processFlag) {

            /* Get step only if prepared before. */
            if (!$this->constructCurrentQuestionSettings()) {
                return false;
            }

            if (!$this->constructCurrentQuestion()) {
                return false;
            }

        } else {

            /* Get step and save it. */
            if ($this->constructCurrentQuestionSettings()) {
                $this->constructCurrentQuestion();
            } else {
                return false;
            }

            $this->info->save();

        }

        return true;
    }

    /**
     * Method call methods preparing question before rendering.
     */
    public function prepareStep()
    {
        if (!$this->runSetToBuildQuestion()) {
            $this->completeCourse();
            return false;
        }
        return true;
    }

    /**
     * Method
     * @throws \Exception
     */
    private function completeCourse()
    {
        /** @var Result $model */
        $model = new Result();
        $model->createByCourseProcessObject();

        $this->info;
        /* NOTE: **************************************************************
         * Why delete it info?
         * Keep only non-completed coursesProcess for more clearly statistic
         * Keep only full-completed coursesResults for more clearly statistic
         */
        $this->info->delete();

        // setCompletedInfo
    }

    /**
     * Method realize logic for checking post and
     * clean current information about question at Database.
     *
     * In some case scripts break by redirects to page "Continue".
     *
     * @return void
     * @throws \Exception
     */
    public function checkPost()
    {
        if (!$this->isAnswer) {
            return;
        }

        if (!$this->runSetToBuildQuestion(true)) {
            /* If something wrong - then redirect for restarting step.
             * There is redirect what blocks script below. */
            $this->redirectToContinue();
            /* return false; */
        }

        /** @var BaseGeneralForm $model */
        $model = &$this->currentStepQuestionLayer->content->model;

        if (!($model->load(Yii::$app->request->post()))) {
            $this->redirectToContinue();
            /* return false; */
        }

        /* checkAnswers. */
        $model->check();

        /**
         * Method helper increment step at Course information and clean
         * Question information to Databases.
         *
         * Clear last question and increment step
         *
         * @see CoursesProcess          - Object what contain all points of Course.
         * @see BaseGeneralForm         - Current class.
         */
        $this->info->getContent()->temporaryQuestionSettings = null;
        $this->info->current_step++;
        $this->info->update();
    }

    /**
     * Method construct settings if empty.
     * Settings will be set to "content" property of CoursesProcess.
     *
     * When complete successful return true.
     *
     * If no more steps then return false.
     *
     * @return boolean.
     */
    private function constructCurrentQuestionSettings()
    {
        if ($this->info->getContent()->temporaryQuestionSettings !== null) {
            /* information set yet before */
            return true;
        }


        if ($this->getTypePointer() === null) {
            /* no more steps need return signal for stop and run complete it. */
            return false;
        }

        $this->info->getContent()->temporaryQuestionSettings = json_decode(
            json_encode([
                'typePointer' => $this->getTypePointer(),
                'className' => $this->getClassName(),
                'templateName' => $this->getSettings()['type'],
                'content' => [
                    'question' => $this->getSettings()['task']['text'],
                    'nextStep' => (Yii::$app->course->info->current_step + 1),
                ],
                'task' => $this->getSettings()['task']
            ]));

        return true;
    }

    /**
     * Method return false if temporaryQuestionSettings not exist.
     * @return boolean.
     */
    private function constructCurrentQuestion()
    {
        if ($this->info->getContent()->temporaryQuestionSettings === null) {
            return false;
        }

        $className = $this->info->getContent()->temporaryQuestionSettings
            ->className;
        
        /** @var BaseGeneralForm $model */
        $model = new $className();

        $this->currentStepQuestionLayer = json_decode(json_encode([
            'templateName' => $this->info->getContent()
                ->temporaryQuestionSettings->templateName,
            'content' => [
                'nextStep' => $this->info->getContent()
                    ->temporaryQuestionSettings->content->nextStep
            ]
        ]));

        $model->prepareContent(
            $this->currentStepQuestionLayer->content,
            $this->info->getContent()->temporaryQuestionSettings
        );


        return true;
    }

    /**
     * Method return next step pointer.
     * @return integer
     */
    private function getTypePointer()
    {
        if ($this->info->getContent()->temporaryQuestionSettings !== null) {
            
            $this->currentTypePointer = $this->info->getContent()
                ->temporaryQuestionSettings->typePointer;
            
        }

        if ($this->currentTypePointer === null) {
            $this->currentTypePointer = $this->info->constructNextStep();
        }

        return $this->currentTypePointer;
    }

    /**
     * Return settings for current type of Question.
     * @return array
     */
    private function getSettings()
    {
        if ($this->currentQuestionSetting === null) {
            $this->currentQuestionSetting =
                $this->settings[$this->getTypePointer()];
        }

        return $this->currentQuestionSetting;
    }

    /**
     * Method return path to view.
     * @return string
     */
    public function getTemplatePath()
    {
        return 'forms/types/' . $this->currentStepQuestionLayer->templateName;
    }

    /**
     * Method return array with content for render-functions.
     * @return mixed
     */
    public function getContent()
    {
        return (array)$this->currentStepQuestionLayer->content;
    }

    /**
     * Just redirect with blocking all next script.
     *
     * That redirect uses when Course of Student is expired.
     *
     * And Student at that page can run start new Course.
     */
    protected function redirectCaseExpired()
    {
        $this->controllerRef->redirect(
            Url::to([static::LINK_TO_COURSE_EXPIRED]),
            307
        )->send();
    }

    /**
     * Just redirect with blocking all next script.
     *
     * That redirect uses when Course of Student still alive.
     *
     * And Student at that page can run continue Course.
     */
    protected function redirectToContinue()
    {
        $this->controllerRef->redirect(
            Url::to([static::LINK_TO_COURSE_CONTINUE]),
            307
        )->send();
    }

    /**
     * Just redirect with blocking all next script.
     */
    protected function redirectToFirstTask()
    {
        $this->controllerRef
            ->redirect(Url::to(['course/step', 'step' => 2]))
            ->send();
    }

    /**
     * @return string
     */
    private function getClassName()
    {
        /** @var string $t */
        $t = '';

        if (isset($this->getSettings()['model'])) {
            $t = $this->getSettings()['model'];
        }

        $t .= ucfirst($this->getSettings()['type']);
        return static::FORMS_NAMESPACE_PATH . $t . 'Form';
    }
}