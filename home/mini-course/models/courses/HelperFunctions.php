<?php
/**
 * Project:     mini-course
 * File:        HelperFunctions.php
 * Author:      planet17
 * DateTime:    M11.D07.2016 2:16 AM
 */

namespace app\models\courses;


class HelperFunctions
{
    /**
     * @const TYPE_WEEK_DAY_NAME_FULL
     */
    const TYPE_WEEK_DAY_NAME_FULL = 1;

    /**
     * @const TYPE_WEEK_DAY_NAME_FULL
     */
    const WEEK_LENGTH = 7;

    public static function generateWeekDaysSet(
        $length = 1,
        $modeShuffle = true,
        $modeToday = 1,
        $dayNumber = null
    )
    {
        $allDays = range(0, 6);
        $mergeDay = [];

        switch ($modeToday) {
            case 2:
                if (!$dayNumber) {
                    $dayNumber = $dayNumber = (integer)date('N') - 1;
                }
                $mergeDay = array_splice($allDays, $dayNumber, 1);
                // no break
            default:
                $numberDaysForDelete = static::WEEK_LENGTH - $length;
        }

        if ($modeShuffle) {
            shuffle($allDays);
        }

        $days = array_merge(
            array_slice($allDays, $numberDaysForDelete),
            $mergeDay
        );


        foreach ($days as &$day) {
            $day = static::processingValueToNameOfWeekDay($day);
        }

        if ($modeShuffle) {
            shuffle($days);
        }

        return $days;
    }
    
    public static function processingValueToNameOfWeekDay($day)
    {
        return jddayofweek($day, static::TYPE_WEEK_DAY_NAME_FULL);
    }
}