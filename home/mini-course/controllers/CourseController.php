<?php
/**
 * Project:     mini-course
 * File:        CourseController.php
 * Author:      planet17
 * DateTime:    M10.D31.2016 1:23 AM
 */

namespace app\controllers;

use app\models\courses\models\CoursesProcess;
use app\models\courses\forms\CustomStartForm;

use app\models\courses\models\Result;
use yii;
use yii\base\Exception;
use yii\filters\AccessControl;

/**
 * Class CourseController.
 *
 * @package app\controllers.
 *
 * @property CoursesProcess|null
 */
class CourseController extends AuthController
{
    /** @var string $defaultAction Override default action */
    public $defaultAction = 'start';

    /**
     * Override beforeAction method.
     *
     * @param yii\base\Action $action
     * @return boolean
     * @throws Exception
     * @throws yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->course->fixBy($action);

        if (!parent::beforeAction($action)) {
            return false;
        }

        return Yii::$app->course->secureValidation();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'start',
                    'step',
                    'complete',
                    'continue-course',
                    'expired'
                ],
                'rules' => [
                    [
                        'actions' =>
                            [
                                'step',
                                'complete',
                                'continue-course',
                                'expired'
                            ],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['start'],
                        'allow' => true,
                        'roles' => ['?', '@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Display Homepage and it page for start New Course and page for login.
     * 
     * @return string
     *
     * @throws \Exception
     */
    public function actionStart()
    {
        $model = new CustomStartForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->check();
        }

        /** @var string $renderedPartTop|boolean */
        if (Yii::$app->user->isGuest) {
            /** @var string|true $renderedPartTop */
            $renderedPartTop = parent::renderPartialLoginPanel();

            if ($renderedPartTop === true) {
                if (Yii::$app->course->secureValidation()) {
                    $model->runConstructor();
                }
            }

        } else {
            $renderedPartTop = $this->renderPartialStartPanel($model);
        }
        
        return $this->render('pages/start', [
            'renderedPartTop' => $renderedPartTop,
            'model' => (new Result())->getLastResults()
        ]);
    }

    /**
     * @param integer $step Number from 2 to 7
     * @return string
     */
    public function actionStep($step)
    {
        Yii::$app->course->checkPost();
        
        if (!Yii::$app->course->prepareStep()) {
            return $this->redirect(yii\helpers\Url::to(['course/complete']));
        }
        
        return $this->render(
            Yii::$app->course->getTemplatePath(),
            Yii::$app->course->getContent()
        );
    }

    /**
     * 
     */
    public function actionComplete()
    {
        /** @var Result $studentLastResults */
        $studentLastResults = (new Result())->getLastResultForCurrentStudent();
        
        return $this->render('pages/complete', [
            'name' => Yii::$app->user->identity->login,
            'results' => $studentLastResults
        ]);
    }

    /**
     * @return string
     */
    public function actionExpired()
    {
        return $this->render('pages/expired');
    }

    /**
     * @return string
     */
    public function actionContinueCourse()
    {
        return $this->render('pages/continue-course', [
            'currentStep' => Yii::$app->course->info->current_step
        ]);
    }

    /**
     * @param $model
     * @return string
     */
    protected function renderPartialStartPanel(&$model)
    {
        return $this->renderPartial('forms/general/form-start', [
            'userLgn' => Yii::$app->user->identity->login,
            'model'   => $model
        ]);
    }
}