<?php

namespace app\controllers;

use yii;
use yii\web\Controller;
/**
 * Class SiteController.
 *
 * Default Yii-controller. I keep it for errorHandler.
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
}
