<?php
/**
 * Project:     mini-course
 * File:        AuthController.php
 * Author:      planet17
 * DateTime:    M10.D31.2016 6:50 PM
 */

namespace app\controllers;

use app\models\students\InForm;

use yii;
use yii\web\Controller;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['in', 'logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['in'],
                        'allow' => true,
                        'roles' => ['?', '@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Login action.
     * 
     * Method return true when login is successful.
     * 
     * At other cases return string of rendered template of the login form.
     * 
     * @return true|string
     */
    public function renderPartialLoginPanel()        
    {
        $model = new InForm();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {               
                return true;
            }

            Yii::$app->session->setFlash('loginError');
        }

        return $this->renderPartial('//auth/form-in', [
            'model' => $model
        ]);
    }
    
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}