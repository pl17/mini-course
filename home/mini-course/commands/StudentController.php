<?php
/**
 * Project:     mini-course
 * File:        StudentController.php
 * Author:      planet17
 * DateTime:    M10.D31.2016 1:13 PM
 */

namespace app\commands;

use app\models\students\Student;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class StudentController
 * @package app\commands
 *
 * @see StudentController::actionIndex
 * @see StudentController::actionAddNew
 */
class StudentController extends Controller
{
    /**
     * @const COMMAND_CREATE_CLEAR_STRING
     */
    const COMMAND_CREATE_CLEAR_STRING = "path-to-yii/yii student/add-new";

    /**
     * @const PARAM_NAME
     */
    const PARAM_NAME = '{%login_string%}';

    /**
     * This command echoes helper for StudentController::class.
     */
    public function actionIndex()
    {
        echo $this->textHelper();
    }

    /**
     * This command create new student, account at application set permission to access for course.
     * @param string $login the login what will been registered to students.
     */
    public function actionAddNew($login)
    {
        /** @var boolean|array $result */
        if (($result = Student::create($login)) !== true) {
            echo $this->textError($result);
        } else {
            echo $this->textSuccess();
        }
    }

    /**
     * @return string
     */
    protected function textSuccess()
    {
        return $this->ansiFormat("Success!\nStudent had been created!\n\n", Console::FG_GREEN);
    }

    /**
     * @param array $errors
     * @return string
     */
    protected function textError(array $errors)
    {
        $errString = '';
        $cnt = 0;

        foreach ($errors as $field) {
            foreach ($field as $error) {
                $errString .= 'Error [#' . ++$cnt . "]:\n" . $error . "\n------\n";
            }
        }

        $headerString = "FAILED!\n" . 'You have ' . count($errors) . " errors:\n";

        $errString =
            $this->ansiFormat($headerString, Console::FG_RED) .
            $this->ansiFormat($errString, Console::FG_YELLOW).  "\n";

        return $errString;
    }

    /**
     * @return string
     */
    protected function textHelper()
    {
        /** @var integer $min */
        $min = $this->ansiFormat(Student::LOGIN_STRING_LENGTH_MIN, Console::FG_RED);
        /** @var integer $max */
        $max = $this->ansiFormat(Student::LOGIN_STRING_LENGTH_MAX, Console::FG_RED);
        $paramString = $this->ansiFormat(static::PARAM_NAME, Console::FG_RED);
        /** @var string $commandString */
        $commandString = '$ ' . static::COMMAND_CREATE_CLEAR_STRING;
        $commandString = "\n\n" . $this->ansiFormat($commandString, Console::FG_BLUE) . ' "' .
            $paramString . '"' . "\n\n";

        return "You can " . $this->ansiFormat('add', Console::BOLD) .
        " new student in that component.\n" .
        "Just use following command:" .
        "{$commandString}" .
        "{$paramString} must contain from {$min} to {$max} symbols.\n\n";
    }
}