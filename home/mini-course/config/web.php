<?php
/** @var array $params */
$params = require(__DIR__ . '/params.php');
/** @var array $config */
$config = [
    'id' => 'ao.mini-course.main',
    'basePath' => dirname(__DIR__),
    /* My Mysql server pre-set for Moscow time. */
    'timeZone' => 'Europe/Moscow',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'eRseX6b99DO-mxj8O-NxeuwbY6tXIFqm',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'loginUrl' => ['/'],
            'identityClass' => 'app\models\students\Student',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern' => '/',
                    'route' => 'course/start'
                ],
                [
                    'pattern' => 'step/<step:[2-9]>',
                    'route' => 'course/step'
                ],
                [
                    'pattern' => '<action>',
                    'route' => 'course/<action>'
                ]
            ],
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' =>
                    YII_ENV_DEV ?
                        ['js' => ['jquery.js']] :
                        [
                            'sourcePath' => null,
                            'js' => [
                                'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js'
                            ]
                        ]
            ],
        ],
        'course' => function () {
            return new app\models\courses\Component();
        },
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
