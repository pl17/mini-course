<?php
/**
 * Project:     mini-course
 * File:        in.php
 * Author:      planet17
 * DateTime:    M10.D31.2016 6:54 PM
 *
 * Partial template of form for Login and button to send it and then go to next step.
 *
 * @var $this yii\web\View
 * @var $model \app\models\students\InForm
 */
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/** @var string $error */
$error = "Your key is wrong! Access restricted!";
/** @var string $formTemplate Html-string template for each input at the form */
$formTemplate = "<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>";?>

<!-- start::form-in -->
<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => [
            'class' => 'form-horizontal'
        ],
        'fieldConfig' => [
            'template' => $formTemplate,
            'labelOptions' => [
                'class' => 'col-lg-1 control-label'
            ],
        ],
    ]
);

if (Yii::$app->session->hasFlash('loginError')) { ?>
    <div class="alert alert-danger"><?=$error;?></div>
<?php }
echo $form->field($model, 'login')->input(
    'text',
    [
        'placeholder' => $model->attributeLabels()['login'],
        'autofocus' => true
    ]
); ?>
<div class="form-group">
    <div class="col-lg-offset-0 col-lg-12">
        <?= Html::submitButton(
            'Next!',
            [
                'class' => 'btn btn-primary',
                'name' => 'login-button'
            ]
        ); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- end::form-in -->
