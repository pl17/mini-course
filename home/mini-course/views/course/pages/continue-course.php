<?php
/**
 * Project:     mini-course
 * File:        continue-course.php
 * Author:      planet17
 * DateTime:    M11.D01.2016 9:47 PM
 *
 * Template for case when your Course expired.
 *
 * @var $this yii\web\View
 *
 * @var $currentStep integer
 *
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Continue your course!';
?>

<!-- start::continue-course -->
<div class="site-continue">
    <h1><?= Html::encode($this->title); ?></h1>
    <div class="alert alert-info">
        You have course now! Just continue it!
    </div>
    <div class="form-group">
        <div class="col-lg-offset-0 col-lg-12">
            <?= Html::a('Continue', Url::to(['step', 'step' => $currentStep]), [
                'class' => 'btn btn-primary',
            ]); ?>
        </div>
    </div>
</div>
<!-- start::continue-course -->
