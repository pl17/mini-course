<?php
/**
 * Project:     mini-course
 * File:        complete.php
 * Author:      planet17
 * DateTime:    M11.D05.2016 5:20 PM
 *
 * Template for case when your watch last result of Course.
 *
 * @var $this yii\web\View
 *
 * @var $name string
 * @var $results \app\models\courses\models\Result|null
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Congratulations!';
?>

<!-- start::complete -->
<div class="site-complete">
    <h1><?= Html::encode($this->title); ?></h1>
    <hr>
    <div class="result-table">
        <?php if ($results === null) { ?>
            <p><span>hacker detected!</span></p>
        <?php } else { ?>
            <p><span><?= $name; ?></span>, you had finished course!</p>
            <hr>
            <p>Your last score: <span><?= $results->points; ?> points</span>.</p>
            <hr>
            <p>The total time of your course: <span><?= $results->time_spent; ?> seconds</span>.</p>
            <hr>
            <div class="form-group">
                <div class="col-lg-offset-0 col-lg-12">
                    <?= Html::a('Back to Start', Url::to(['course/start']), [
                        'class' => 'btn btn-primary',
                    ]); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<!-- end::complete -->
