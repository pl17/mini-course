<?php
/**
 * Project:     mini-course
 * File:        expired.php
 * Author:      planet17
 * DateTime:    M11.D01.2016 9:48 PM
 * 
 * Template for case when your Course expired.
 *
 * @var $this yii\web\View
 *
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Your course expired! Take new!';
?>

<!-- start::expired -->
<div class="site-expired">
    <h1><?= Html::encode($this->title); ?></h1>
    <div class="alert alert-info">
        Your course had expired now! You can start new!
    </div>
    <div class="form-group">
        <div class="col-lg-offset-0 col-lg-12">
            <?= Html::a('Continue', Url::home(), [
                'class' => 'btn btn-danger',
            ]); ?>
        </div>
    </div>
</div>
<!-- end::expired -->
