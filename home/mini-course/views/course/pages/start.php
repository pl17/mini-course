<?php
/**
 * Project:     mini-course
 * File:        start.php
 * Author:      planet17
 * DateTime:    M10.D31.2016 6:08 PM
 *
 * Template for Homepage.
 *
 * @var $this yii\web\View
 * @var $renderedPartTop string
 * @var $model \app\models\courses\models\Result[]
 *
 */
use yii\bootstrap\Html;

$this->title = 'Start course!';
?>

<!-- start::start -->
<div class="site-index">
    <div class="content-top">
        <h1><?= Html::encode($this->title); ?></h1>
        <hr>
        <?= $renderedPartTop; ?>
    </div>
    <hr>
    <div class="content-results">
        <h3>Last results:</h3>
        <hr>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-condensed last-results">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Score</th>
                    <th>Time spent</th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($model)) { ?>
                    <?php foreach ($model as $result) { ?>
                        <tr>
                            <td><?= $result->getStudent()->login; ?></td>
                            <td><?= $result->points; ?></td>
                            <td><?= $result->time_spent; ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="3" style="text-align: center;">No results found.</td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- end::start -->
