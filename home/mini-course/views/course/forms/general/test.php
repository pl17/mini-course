<?php
/**
 * Project:     mini-course
 * File:        test.php
 * Author:      planet17
 * DateTime:    M11.D03.2016 8:53 PM
 * 
 * @var $this yii\web\View
 * @var $model app\models\courses\forms\TypeTestGeneralForm
 * @var $nextStep integer
 *
 */
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Test!';

/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'action' => Url::to(['step', 'step' => $nextStep]),
    'id' => 'start-form',
    'options' => ['class' => 'form-horizontal']
]);
echo $form->field($model, 'test')->hiddenInput(); ?>
    <div class="form-group">
        <div class="col-lg-offset-0 col-lg-12">
            <?= Html::submitButton(
                'Test!',
                ['class' => 'btn btn-primary', 'name' => 'start-button']
            ); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>