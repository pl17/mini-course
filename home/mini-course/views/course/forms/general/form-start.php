<?php
/**
 * Project:     mini-course
 * File:        form-start.php
 * Author:      planet17
 * DateTime:    M11.D02.2016 4:12 PM
 *
 * Partial template of header with your name and button for next step.
 *
 * @var $this yii\web\View
 * @var $userLgn string
 * @var $model \app\models\courses\forms\CustomStartForm
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var string $formTemplate Html-string template for each input at the form */
$formTemplate = "<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>"; ?>

<!-- start::form-start::your-login -->
<div class="your-login">
    <p>Your login: <strong><?= $userLgn; ?></strong></p>
</div>
<!-- end::form-start::your-login -->

<!-- start::form-start::form -->
<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'id' => 'start-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => $formTemplate,
        'labelOptions' => ['class' => 'col-lg-1 control-label']
    ]]);

echo $form->field($model, 'hiddenJack')->hiddenInput(['value' => true]); ?>
<div class="form-group">
    <div class="col-lg-offset-0 col-lg-12">
        <?= Html::submitButton(
            'Next!',
            ['class' => 'btn btn-primary', 'name' => 'start-button']
        ); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- end::form-start::form -->
