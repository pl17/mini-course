<?php
/**
 * Project:     mini-course
 * File:        checkbox.php
 * Author:      planet17
 * DateTime:    M11.D04.2016 3:01 AM
 *
 * @var $this yii\web\View
 * @var $model \app\models\courses\forms\QuestionTodayRadioForm
 * @var $nextStep integer
 * @var $question string
 * @var $helper string
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var string $formTemplate Html-string template for each input at the form */
$formTemplate = "<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>"; ?>

<!-- start::checkbox -->
<h3><?= $question; ?></h3>

<?php

/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'action' => Url::to(['step', 'step' => $nextStep]),
    'id' => 'checkbox-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => $formTemplate,
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ]]);

echo $form->field($model, 'selected', [
    'inline' => false,
    'enableLabel' => false
])->checkboxList(
    Yii::$app->course->info->getContent()->temporaryQuestionSettings->content->options,
    ['class' => 'btn-group', 'data-toggle' => 'buttons', 'unselect' => null]
);
?>
<div class="form-group">
    <div class="col-lg-offset-0 col-lg-12">
        <?= Html::submitButton(
            'Next!',
            ['class' => 'btn btn-primary', 'name' => 'start-button']
        ); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- end::checkbox -->

