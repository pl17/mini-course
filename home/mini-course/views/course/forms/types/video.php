<?php
/**
 * Project:     mini-course
 * File:        video.php
 * Author:      planet17
 * DateTime:    M11.D04.2016 2:28 PM
 *
 * @var $this yii\web\View
 * @var $model \app\models\courses\forms\ReadForm
 * @var $videoLink string
 * @var $nextStep integer
 * @var $question string
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var string $formTemplate Html-string template for each input at the form */
$formTemplate = "<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>"; ?>
<!-- start::video -->
<h3><?= $question; ?></h3>

<!-- start::video::video -->
<div class="videoWrapper col-lg-offset-0 col-lg-12">
    <div id="player"></div>
    <script>
        /**
         * @description Object type of Youtube API.
         * @type {*}
         * @name Player
         */
        var Player;

        /**
         * @description Duration of video at seconds.
         * @type {Number}
         * @name vDuration
         */
        var vDuration = 0;

        /**
         * @description Boolean flag. When watching start it set by true.
         * @type {boolean}
         * @name onWatchingFlag
         */
        var onWatchingFlag = false;

        /**
         * @description Boolean flag.
         * When timer complete blocking attempt for changes it set by true.
         * @type {boolean}
         * @name onTimeSpent
         */
        var onTimeSpent = false;

        var firstScriptTag,
            tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";

        firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        /**
         * @function onYouTubeIframeAPIReady
         * @description Function calls by Youtube API.
         */
        function onYouTubeIframeAPIReady() {
            Player = new YT.Player('player', {
                height: '270', width: '480',
                videoId: '<?= $videoLink; ?>',
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        /**
         * @function onPlayerReady
         * @description Function call by eventHandler (onReady) at Player.
         * @param e {event}
         */
        function onPlayerReady(e) {
            vDuration = parseInt(Player.getDuration() * 0.95);
        }

        /**
         * @function onPlayerStateChange
         * @description Function call by eventHandler (onStateChange) at Player.
         * @param e {event}
         */
        function onPlayerStateChange(e) {
            /* Set delay when Player start playing. */
            if (e.data == 1) {
                if (!onWatchingFlag) {
                    onWatchingFlag = true;
                    setTimeout(changeLookResult, (vDuration * 1000));
                }
            }

            /* Set value of watching result to TRUE! */
            if (e.data == 0) {
                if (onTimeSpent) {
                    $('input[name="VideoForm[hiddenJack]"]').val(1);
                }
            }
        }

        /**
         * @function changeLookResult
         * @description Function call by eventHandler (onStateChange) at Player.
         * It call at event when state is watching after and not immediately, over delay.
         */
        function changeLookResult() {
            onTimeSpent = true;
        }
    </script>
</div>

<!-- end:video:video -->

<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'action' => \yii\helpers\Url::to(['step', 'step' => $nextStep]),
    'id' => 'video-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => $formTemplate,
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ]]);
echo $form->field($model, 'hiddenJack')->hiddenInput(['value' => 0]); ?>

<div class="form-group">
    <div class="col-lg-offset-0 col-lg-12">
        <?= Html::submitButton(
            'Next!',
            ['class' => 'btn btn-primary', 'name' => 'start-button']
        ); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<!-- end::video -->