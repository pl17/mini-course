# Mini Course


#### [Public demo](http://7km-najar.com.ua/nnj/tsp/ao/)


## Project is test specification.

Requirements and descriptions for that application you can read at `TEST-SPECIFICATION.pdf`.

Application based at Basic Application Yii2 framework.

You can read standard Yii2-basic-app `README.md` at `home/mini-course/`.



CONTENTS:
---------

* [PROJECT's STRUCTURE](#markdown-header-projects-structure)
* [APPLICATION's STRUCTURE](#markdown-header-applications-structure)
* [REQUIREMENTS](#markdown-header-something-like-requirements)
* [INSTALLATION](#markdown-header-installation)



PROJECT's STRUCTURE
-------------------


      commands/           contains Console commands for giving access for user.
      
        StudentConsole    single controller contains actions what you can use
                    via console.
        
      config/             contains application configurations
      
        db                must contains configuration for Database server.
        dist-db           example of how must look db.php
        web               have general Web configuration of application.
            
      controllers/        contains Web controller classes
      
        AuthController    controller contains some behaviours for
                    authentication.
                    
        CourseController  controller contains all actions for works with
                    Courses.
                    
        SiteController    controller contains some default behaviours for
                    application.
      
      migrations/         contains migration classes what uses like tool for
                    automation of changes in the database.
                    
      models/             contains model classes
        
        courses/          contains model classes (also have so much holyshit
                    scripts now). :-)
        
            forms/        contains models what behind the all questions form.
            
            models/       contains models (CoursesProcess, Result) for works
                    with entities and one model just for template what uses
                    inside the CoursesProcess entity.
            
            Component
                    Class what had been created for uses like main Model for
                    all files in that directories, and also for uses it like
                    Component ().
            
            HelperFunctions
                    Class had been created with idea to separated some of
                    functional to Helpers. Now it contains functionality
                    with generating set of week, and converted integer value
                    to week day name.
                    
            settings      contains preset of configurations for some Forms.
                    Some of Forms uses full settings what file contains, some
                    of them - uses only part.
        
        students/         contains model for students: first model - for works
                    with entities and authentications; second - model behind the
                    login form.
      
      views/              contains view files for the Web application


|[>> back to contents <<](#markdown-header-contents)|
|:-------------------------------------------------:|
|                                                   |


APPLICATION's STRUCTURE
-----------------------


      dump/               directory with of sql
            
      home/mini-course/   root application directory
      
      media/              directory where keep all media files not for
                    project, but for demonstrate something at project.
      
      public/             public directory with entry scripts,      
                    and public files. Usual place for project, but not
                    for current server at Demo-link
                    
      public/nnj/tsp/ao/  directory the same like `/public`. Just      
                    duplicate for version application uploaded to
                    server at Demo-link


More information about structure you can get at Yii-sites.

* [Yii2-app-basic](https://github.com/yiisoft/yii2-app-basic)
* [Structure Overview](http://www.yiiframework.com/doc-2.0/guide-structure-overview.html)
* Or just read file `home/mini-course/README.md`.


|[>> back to contents <<](#markdown-header-contents)|
|:-------------------------------------------------:|
|                                                   |



SOMETHING LIKE REQUIREMENTS
---------------------------


My local PC have:

| OS        | Server (one of that)  || Database     | Language interpreter |
|:---------:|-----------:|:----------|:------------:|:--------------------:|
| Ubuntu    | Apache     | Nginx     | MySQL        | PHP                  |
| 16.04 LTS | 2.4        | 1.4       | 5.6          | 5.7                  |



|[>> back to contents <<](#markdown-header-contents)|
|:-------------------------------------------------:|
|                                                   |



INSTALLATION
------------


1) Clone or download archive.

2) Prepare your local or production server. Set-up servers (apache/nginx, hosts) and path to projects.
I made `/public` directory like root for access from out.

3) Install external libraries via [Composer](http://getcomposer.org/doc/00-intro.md#installation-nix).
 
**NOTE:**
- If something will be wrong, maybe it will be cause you need use following command, before installing
other. But use it only one time for composer at PC. If you use it before, you does not need it. More
information you can get at instructions at Yii2 framework.
  
  
 
```sh
$ php composer global require "fxp/composer-asset-plugin:~1.1.0"
```

[Yii2 framework](http://getcomposer.org/doc/00-intro.md#installation-nix)

4) Create your database or if exist just prepare configuration at `/home/mini-course/config/db.php`.
 Example for it is `/home/mini-course/config/dist-db.php`.
 
5) Use following command at root-directory of project:

```sh
$ home/mini-course/yii migrate
```

It will prepare all tables at database.


Or, another way. Via any DB-manager on your taste, import the
dump from the `demo_template.sql` what inside `/dump` directory.


6) Also you need check permissions at directories:

Following commands will helps you had been sure with permissions.

```sh
$ chmod 777 home/mini-course/runtime
$ chmod 777 public/assets
$ chmod 777 public/nnj/tsp/ao/assets
```

7) Give some students access to your project.

You can try add it via DB-manager or use bash for it.

In the database: information about the students kept in the table "Students".

```json
{
    "id": "ID of Student (Autoincrement)",
    
    "login": "That key what student need when login to application. Also we
        does not keep the other name, so it uses for name too.",
    
    "auth_key": "Unique generator key it need for students authorizations. Keep
        it null when you create Student. When Student will login into
        application, it will be every time refreshing.",
    
    "updated_at": "Time of last record changed.",
    
    "created_at": "Time when Student had been created."
}
```

So when you created it manually you need write only: "login", and set
"updated_at" and "created_at" by current time.

Second and better way to use bash-console commands for it:

```sh
$ home/mini-course/yii student/add-new "demo-student"
```

It is working great if you have access to use it:

![9301bcdc6e.png](https://bitbucket.org/repo/7qajz6/images/4097219939-9301bcdc6e.png)


---




|[>> back to contents <<](#markdown-header-contents)|
|:-------------------------------------------------:|
|                                                   |



---


**NOTES:**
- Be careful when set-up application Timezone. In all models uses behaviours what set time at Databases-side, not PHP-side. So it a reason why application must been set-up like Databases.
- About `login` in the `Student` table: that way what uses for public display login is not correct, it is only for demo. For WORKS PRODUCTION: if you need do close and secure app, you need change that logic, and add other field to table, what been display instead login. 


---



> Best regards!
>
> -- *N.L.*
