# LITTLE TECHNICAL DETAILS ABOUT APPLICATION

CONTENTS:
---------

* [Video checking](#markdown-header-video)



VIDEO
-----
                                                      
Be care! Checks of the video watching is not secure now!

**Some ways how Student can falsifies result:**

* *Student* can set true value to the **hidden input VideoForm[hiddenJack]**.
* Also *Student* can start watching video and then switch tabs in the browser.
After some time when video has ended, *Student* can switch tab at "Course" and
press button "Next".
* *Student* runs the playing of video, after press "Pause". 
Then *Student* wait time equal duration of the movie. After waiting *Student*
rewinds the video to the end. Event *"Video ended"* will be triggered and set
true to the **input**.
